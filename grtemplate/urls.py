from django.urls import path
from grtemplate import views

urlpatterns = [
    path('api/v1/portal/grtemplate/', views.GrtemplateCreateRead.as_view()),
    path('api/v1/portal/grtemplate/<int:pk>/',
         views.GrtemplateUpdateDelete.as_view()),
    path('api/v1/portal/grtemplate/delete_selected/',
         views.grtemplate_delete_selected),
]