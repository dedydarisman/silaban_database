from datetime import datetime
from django.db import models

today = datetime.today().strftime('%Y-%m-%d %H:%M:%S')


class Grtemplate(models.Model):
    script_name = models.TextField(blank=True, null=True)
    device_pid = models.TextField(blank=True, null=True)

    def __str__(self):
        return '%s %s' % (self.script_name, self.device_pid)
