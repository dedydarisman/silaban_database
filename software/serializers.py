from datetime import datetime
from rest_framework import serializers
from software.models import Software, SoftwareApproval


class SoftwareSerializer(serializers.ModelSerializer):
    # id = serializers.IntegerField(read_only=True)
    # username = serializers.CharField(max_length=100)
    class Meta:
        model = Software
        fields = ['id', 'software_version', 'eos_maintenance',
                  'last_date_of_support', 'technology', 'date_generated', 'generated_by'
                  ]

    def create(self, validated_data):
        """
        Create and return a new `Admin` instance, given the validated data.
        """
        return Software.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Admin` instance, given the validated data.
        """
        instance.software_version = validated_data.get(
            'software_version', instance.software_version)
        instance.eos_maintenance = validated_data.get(
            'eos_maintenance', instance.eos_maintenance)
        instance.last_date_of_support = validated_data.get(
            'last_date_of_support', instance.last_date_of_support)
        instance.technology = validated_data.get(
            'technology', instance.technology)
        instance.date_generated = validated_data.get(
            'date_generated', instance.date_generated)
        instance.generated_by = validated_data.get(
            'generated_by', instance.generated_by)
        instance.save()
        return instance


class SoftwareApprovalSerializer(serializers.ModelSerializer):
    class Meta:
        model = SoftwareApproval
        fields = ['id', 'software_version', 'eos_maintenance',
                  'last_date_of_support', 'technology', 'date_generated', 'generated_by', 'approval_id'
                  ]

    def create(self, validated_data):
        """
        Create and return a new `Admin` instance, given the validated data.
        """
        return SoftwareApproval.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Admin` instance, given the validated data.
        """
        instance.software_version = validated_data.get(
            'software_version', instance.software_version)
        instance.eos_maintenance = validated_data.get(
            'eos_maintenance', instance.eos_maintenance)
        instance.last_date_of_support = validated_data.get(
            'last_date_of_support', instance.last_date_of_support)
        instance.technology = validated_data.get(
            'technology', instance.technology)
        instance.date_generated = validated_data.get(
            'date_generated', instance.date_generated)
        instance.generated_by = validated_data.get(
            'generated_by', instance.generated_by)
        instance.approval_id = validated_data.get(
            'approval_id', instance.approval_id)
        instance.save()
        return instance
