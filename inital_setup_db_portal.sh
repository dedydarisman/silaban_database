#!/bin/bash

# run compose for silaban-db-backend
docker-compose up -d;

secs=$((45))
while [ $secs -gt 0 ]; do
   echo -ne "$secs\033[0K\r"
   sleep 1
   : $((secs--))
done

# setup mysql database db_silaban
docker cp ./create_db_silaban.sql silaban_db:/
docker exec silaban_db /bin/sh -c 'mysql -h localhost -u root -pdispusip2023 </create_db_silaban.sql'

secs=$((10))
while [ $secs -gt 0 ]; do
   echo -ne "$secs\033[0K\r"
   sleep 1
   : $((secs--))
done

# make migration and migrate django db_portal
docker-compose run --rm silaban_be python3 manage.py makemigrations;
docker-compose run --rm silaban_be python3 manage.py migrate;
docker restart silaban_be;
