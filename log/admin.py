from django.contrib import admin
from .models import Log, LogApproval


class LogAdmin(admin.ModelAdmin):
    pass


admin.site.register(Log, LogAdmin)


class LogApprovalAdmin(admin.ModelAdmin):
    pass


admin.site.register(LogApproval, LogApprovalAdmin)
