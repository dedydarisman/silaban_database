from authportal.models import ReqAdmin, Admin
from authportal.serializers import ReqAdminSerializer, AdminSerializer
from rest_framework import generics
from rest_framework.decorators import api_view
import requests
import re
import json

from rest_framework.response import Response
from rest_framework.parsers import JSONParser


@api_view(['POST'])
def check_request(request):
    request_data = JSONParser().parse(request)
    print(request_data)
    return Response({"message": "API for checking request"})


@api_view(['POST'])
def auth_complete(request):
    engineerLevel = ''
    isAdmin = False
    isSuccessAuth = False
    listName = []
    request_data = JSONParser().parse(request)
    request_data = request_data['dataCustomer']
    get_username = request_data["email"]
    url = 'https://mtcm.compnet.co.id/api/auth.php'
    response_compnet = requests.post(url, data=request_data)
    response_compnet = response_compnet.text
    if 'true' in response_compnet:
        isSuccessAuth = True
    else:
        isSuccessAuth = False
    renameEmail = request_data['email']
    renameEmail = re.findall('(.*)@', renameEmail)
    try:
        renameEmail = renameEmail[0].split('.')
    except:
        renameEmail = [renameEmail]
    for enum, rename in enumerate(renameEmail):
        upperCase = ''
        splitRename = list(rename)
        for enum, char in enumerate(splitRename):
            if enum == 0:
                char = char.upper()
                upperCase = char
            else:
                upperCase = upperCase+char
        listName.append(upperCase)
    # admin section
    try:
        admin = Admin.objects.get(username=get_username)
        serializer = AdminSerializer(admin)
        isAdmin = True
    except:
        isAdmin = False
    # check engineer section
    url = "https://mtcm.compnet.co.id/api/engineer.php"
    response_compnet = requests.get(url)
    response_compnet = response_compnet.text
    response_compnet = json.loads(response_compnet)
    response_compnet = response_compnet["engineer"]
    response = {}
    for engineer in response_compnet:
        if engineer["engineer_email"] == get_username:
            engineerLevel = engineer["engineer_level"]
    response = {
        'isAdmin': isAdmin,
        'condition': isSuccessAuth,
        'email': get_username,
        'renameuser': (' ').join(listName),
        'level': engineerLevel,
    }
    return Response(response)


@api_view(['POST'])
def auth_user(request):
    isSuccessAuth = False
    listName = []
    request_data = JSONParser().parse(request)
    request_data = request_data['dataCustomer']
    url = 'https://mtcm.compnet.co.id/api/auth.php'
    response_compnet = requests.post(url, data=request_data)
    response_compnet = response_compnet.text
    if 'true' in response_compnet:
        isSuccessAuth = True
    else:
        isSuccessAuth = False
    renameEmail = request_data['email']
    renameEmail = re.findall('(.*)@', renameEmail)
    try:
        renameEmail = renameEmail[0].split('.')
    except:
        renameEmail = [renameEmail]
    for enum, rename in enumerate(renameEmail):
        upperCase = ''
        splitRename = list(rename)
        for enum, char in enumerate(splitRename):
            if enum == 0:
                char = char.upper()
                upperCase = char
            else:
                upperCase = upperCase+char
        listName.append(upperCase)
    response = {
        'isAdmin': False,
        'condition': isSuccessAuth,
        'renameuser': (' ').join(listName),
        'email': request_data['email']
    }
    return Response(response)


@api_view(['POST'])
def auth_admin(request):
    listName = []
    request_data = JSONParser().parse(request)
    request_data = request_data['dataCustomer']
    get_username = request_data["email"]
    renameEmail = get_username
    renameEmail = re.findall('(.*)@', renameEmail)
    try:
        renameEmail = renameEmail[0].split('.')
    except:
        renameEmail = [renameEmail]
    for enum, rename in enumerate(renameEmail):
        upperCase = ''
        splitRename = list(rename)
        for enum, char in enumerate(splitRename):
            if enum == 0:
                char = char.upper()
                upperCase = char
            else:
                upperCase = upperCase+char
        listName.append(upperCase)
    try:
        admin = Admin.objects.get(username=get_username)
        serializer = AdminSerializer(admin)
        print((' ').join(listName))
        response = {
            'isAdmin': True,
            'condition': True,
            'renameuser': (' ').join(listName),
            'email': get_username
        }
        return Response(response)
    except:
        response = {
            'isAdmin': False,
            'condition': False,
            'renameuser': (' ').join(listName),
            'email': get_username
        }
        return Response(response)


@api_view(['POST'])
def check_engineer(request):
    request_data = JSONParser().parse(request)
    engineer_email = request_data['dataCustomer']['email']
    # print(request_data)
    url = "https://mtcm.compnet.co.id/api/engineer.php"
    response_compnet = requests.get(url)
    response_compnet = response_compnet.text
    response_compnet = json.loads(response_compnet)
    response_compnet = response_compnet["engineer"]
    response = {}
    for engineer in response_compnet:
        if engineer["engineer_email"] == engineer_email:
            # print(engineer["engineer_name"])
            # print(engineer["engineer_level"])
            try:
                request_data = request_data['dataCustomer']
                get_username = request_data["email"]
                admin = Admin.objects.get(username=get_username)
                serializer = AdminSerializer(admin)
                response = {
                    'isAdmin': True,
                    'condition': True,
                    'email': engineer_email,
                    'renameuser': engineer["engineer_name"],
                    'level': engineer["engineer_level"],
                }
            except:
                response = {
                    'isAdmin': False,
                    'condition': True,
                    'email': engineer_email,
                    'renameuser': engineer["engineer_name"],
                    'level': engineer["engineer_level"],
                }
    # print(response_compnet)

    return Response(response)


@api_view(['POST'])
def check_duplicate_req_admin(request):
    request_data = JSONParser().parse(request)
    get_username = request_data["username"]
    queryset = ReqAdmin.objects.filter(
        username=get_username)
    if queryset.exists():
        response = {
            "check_req_admin": False,
            "message": "User already sent request"
        }
        print(response)
        return Response(response)
    else:
        response = {
            "check_req_admin": True,
            "message": "User has not sent any request"
        }
        print(response)
        return Response(response)


class ReqAdminCreateRead(generics.ListCreateAPIView):
    queryset = ReqAdmin.objects.all().order_by('id').reverse()
    serializer_class = ReqAdminSerializer

    def perform_create(self, serializer):
        queryset = ReqAdmin.objects.filter(
            username=self.request.data["username"])
        if queryset.exists():
            raise ValidationError('Duplicate Detected !!!')
        serializer.save(username=self.request.data["username"])


class ReqAdminUpdateDelete(generics.RetrieveUpdateDestroyAPIView):
    queryset = ReqAdmin.objects.all()
    serializer_class = ReqAdminSerializer


class AdminCreateRead(generics.ListCreateAPIView):
    queryset = Admin.objects.all().order_by('id').reverse()
    serializer_class = AdminSerializer

    def perform_create(self, serializer):
        print(self.request.data)
        queryset = Admin.objects.filter(
            username=self.request.data["username"])
        if queryset.exists():
            raise ValidationError('Duplicate Detected !!!')
        serializer.save(username=self.request.data["username"])


class AdminUpdateDelete(generics.RetrieveUpdateDestroyAPIView):
    queryset = Admin.objects.all()
    serializer_class = ReqAdminSerializer
