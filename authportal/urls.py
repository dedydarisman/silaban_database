from django.urls import path
from authportal import views

urlpatterns = [
    path('api/v1/portal/auth/', views.auth_user),
    path('api/v1/portal/auth_admin/', views.auth_admin),
    path('api/v1/portal/auth_complete/', views.auth_complete),
    path('api/v1/portal/check_engineer/', views.check_engineer),
    path('api/v1/portal/check_duplicate_req_admin/',
         views.check_duplicate_req_admin),
    path('api/v1/portal/req_admin/', views.ReqAdminCreateRead.as_view()),
    path('api/v1/portal/req_admin/<int:pk>/',
         views.ReqAdminUpdateDelete.as_view()),
    path('api/v1/portal/admin/', views.AdminCreateRead.as_view()),
    # path('api/v1/portal/admin/', views.check_request),
    path('api/v1/portal/admin/<int:pk>/',
         views.AdminUpdateDelete.as_view()),
]
