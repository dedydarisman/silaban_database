from django.db import models


class ReqAdmin(models.Model):
    username = models.CharField(max_length=100, blank=False)

    def __str__(self):
        return '%s' % (self.username)


class Admin(models.Model):
    username = models.CharField(max_length=100, blank=False)

    def __str__(self):
        return '%s' % (self.username)
