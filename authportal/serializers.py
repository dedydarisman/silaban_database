from rest_framework import serializers
from authportal.models import ReqAdmin, Admin


class ReqAdminSerializer(serializers.ModelSerializer):
    # id = serializers.IntegerField(read_only=True)
    # username = serializers.CharField(max_length=100)
    class Meta:
        model = ReqAdmin
        fields = ['id', 'username']

    def create(self, validated_data):
        """
        Create and return a new `Admin` instance, given the validated data.
        """
        return ReqAdmin.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Admin` instance, given the validated data.
        """
        instance.username = validated_data.get('username', instance.username)
        instance.save()
        return instance


class AdminSerializer(serializers.ModelSerializer):
    # id = serializers.IntegerField(read_only=True)
    # username = serializers.CharField(max_length=100)
    class Meta:
        model = Admin
        fields = ['id', 'username']

    def create(self, validated_data):
        """
        Create and return a new `Admin` instance, given the validated data.
        """
        return Admin.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Admin` instance, given the validated data.
        """
        instance.username = validated_data.get('username', instance.username)
        instance.save()
        return instance
