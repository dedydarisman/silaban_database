# Generated by Django 3.1.6 on 2021-02-26 02:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('hardware', '0015_auto_20210226_0240'),
    ]

    operations = [
        migrations.CreateModel(
            name='HardwareApproval',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('device_pid', models.TextField(blank=True, null=True)),
                ('end_of_support', models.TextField(blank=True, null=True)),
                ('date_generated', models.TextField(blank=True, default='2021-02-26 02:46:45')),
                ('generated_by', models.TextField(blank=True, null=True)),
                ('approval_id', models.IntegerField()),
            ],
        ),
        migrations.AlterField(
            model_name='hardware',
            name='date_generated',
            field=models.TextField(blank=True, default='2021-02-26 02:46:45'),
        ),
    ]
