from datetime import datetime
from rest_framework import serializers
from hardware.models import Hardware, HardwareApproval


class HardwareSerializer(serializers.ModelSerializer):
    # id = serializers.IntegerField(read_only=True)
    # username = serializers.CharField(max_length=100)
    class Meta:
        model = Hardware
        fields = ['id', 'device_pid', 'end_of_support', 'technology',
                  'date_generated', 'generated_by']

    def create(self, validated_data):
        """
        Create and return a new `Admin` instance, given the validated data.
        """
        return Hardware.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Admin` instance, given the validated data.
        """
        instance.device_pid = validated_data.get(
            'device_pid', instance.device_pid)
        instance.end_of_support = validated_data.get(
            'end_of_support', instance.end_of_support)
        instance.technology = validated_data.get(
            'technology', instance.technology)
        instance.date_generated = validated_data.get(
            'date_generated', instance.date_generated)
        instance.generated_by = validated_data.get(
            'generated_by', instance.generated_by)
        instance.save()
        return instance


class HardwareApprovalSerializer(serializers.ModelSerializer):
    class Meta:
        model = HardwareApproval
        fields = ['id', 'device_pid', 'end_of_support', 'technology',
                  'date_generated', 'generated_by', 'approval_id']

    def create(self, validated_data):
        """
        Create and return a new `Admin` instance, given the validated data.
        """
        return HardwareApproval.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Admin` instance, given the validated data.
        """
        instance.device_pid = validated_data.get(
            'device_pid', instance.device_pid)
        instance.end_of_support = validated_data.get(
            'end_of_support', instance.end_of_support)
        instance.technology = validated_data.get(
            'technology', instance.technology)
        instance.date_generated = validated_data.get(
            'date_generated', instance.date_generated)
        instance.generated_by = validated_data.get(
            'generated_by', instance.generated_by)
        instance.approval_id = validated_data.get(
            'approval_id', instance.approval_id)
        instance.save()
        return instance
