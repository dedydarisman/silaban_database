from django.shortcuts import render
from rest_framework.decorators import api_view
from rest_framework.response import Response
import requests
import re


@api_view(['GET'])
def port_detection(request):
    ports = []
    for port in range(1, 10):
        # if port == 3:
        #     continue
        try:
            url = "https://compnetmtc.com:510" + \
                str(port)+"/port_check"
            # url = "http://localhost:510"+str(port)+"/port_check"
            response = requests.get(url, timeout=1)
            response = response.text
            response = response.rstrip()
            response = re.sub('"', '', response)
            ports.append({
                "port": "510"+str(port),
                "user": response,
                "url": "https://compnetmtc.com:510"+str(port)
            })
        except:
            ports.append({
                "port": "510"+str(port),
                "user": "Unreachable",
                "url": "Unreachable"
            })
    for port in range(10, 11):
        try:
            url = "https://compnetmtc.com:51" + \
                str(port)+"/port_check"
            # url = "http://localhost:51"+str(port)+"/port_check"
            response = requests.get(url, timeout=1)
            response = response.text
            response = response.rstrip()
            response = re.sub('"', '', response)
            ports.append({
                "port": "51"+str(port),
                "user": response,
                "url": "https://compnetmtc.com:51"+str(port)
            })
        except:
            ports.append({
                "port": "51"+str(port),
                "user": "Unreachable",
                "url": "Unreachable"
            })
    print(ports)
    # request_data = JSONParser().parse(request)
    # print(request_data)
    # return Response({"message": "API for checking request"})
    return Response(ports)
