from django.apps import AppConfig


class PortdetectionConfig(AppConfig):
    name = 'portdetection'
