from django.urls import path
from portdetection import views

urlpatterns = [
    path('api/v1/portal/portdetection/', views.port_detection),
]
