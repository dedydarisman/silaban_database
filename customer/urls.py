from django.urls import path
from customer import views

urlpatterns = [
    path('api/v1/portal/customer/', views.CustomerCreateRead.as_view()),
    path('api/v1/portal/customer/<int:pk>/',
         views.CustomerUpdateDelete.as_view()),
    path('api/v1/portal/customer/delete_selected/',
         views.customer_delete_selected),
]
