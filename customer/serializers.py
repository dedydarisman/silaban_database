from datetime import datetime
from rest_framework import serializers
from customer.models import Customer


class CustomerSerializer(serializers.ModelSerializer):
    # id = serializers.IntegerField(read_only=True)
    # username = serializers.CharField(max_length=100)
    class Meta:
        model = Customer
        fields = ['id', 'customer_name', 'device_name',
                  'model', 'card', 'sn', 'version', 'date_generated', 'generated_by'
                  ]

    def create(self, validated_data):
        """
        Create and return a new `Admin` instance, given the validated data.
        """
        return Customer.objects.create(**validated_data)

    def update(self, instance, validated_data):
        """
        Update and return an existing `Admin` instance, given the validated data.
        """
        instance.customer_name = validated_data.get(
            'customer_name', instance.customer_name)
        instance.device_name = validated_data.get(
            'device_name', instance.device_name)
        instance.model = validated_data.get(
            'model', instance.model)
        instance.card = validated_data.get(
            'card', instance.card)
        instance.sn = validated_data.get(
            'sn', instance.sn)
        instance.version = validated_data.get(
            'version', instance.version)
        instance.date_generated = validated_data.get(
            'date_generated', instance.date_generated)
        instance.generated_by = validated_data.get(
            'generated_by', instance.generated_by)
        instance.save()
        return instance
